#include "DataBase.h"

//global variables
int currCount = 0;
int gamesCuont = 0;
string avgTime = "";

/*
this function is the database constructor
input : none
output : none
*/
DataBase::DataBase()
{
	this->_users = vector<tempUser>();
	this->_questions = vector<Question>();
	int rc = 0;
	char* zErrMsg = nullptr;

	rc = sqlite3_open("TriviaDB.db", &this->_db);//open the database

	if (rc)
	{
		cout << "Error opening SQLite3 database: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_close(this->_db);
		throw std::runtime_error("error opening database");
	}

	fillDataFromSQLServer();
	
}
/*
this function is the copy constructor
input: other database instance
output: none
*/
DataBase::DataBase(DataBase& other)
{
	this->_users = other._users;
	this->_db = other._db;
}
/*
this function is the database distructor
input: none
output: none
*/
DataBase::~DataBase()
{
	sqlite3_close(this->_db);
	this->_users.clear();
}

/*
This function is getting the data from the sql server and filling it in the vectors.
Input: None.
Output: None.
*/
void DataBase::fillDataFromSQLServer()
{
	char** resaults = nullptr;
	int rows = 0, cols = 0;
	int rc = 0;
	char* zErrMsg = nullptr;

	rc = sqlite3_exec(this->_db, "select count(game_id) from t_games;", callbackCount, NULL, &zErrMsg);
	this->_gameCtr = gamesCuont;

	if (rc)
	{
		cout << "Error executing SQLite3 query: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
	}

	rc = sqlite3_get_table(this->_db, "SELECT * FROM t_users;", &resaults, &rows, &cols, &zErrMsg);

	if (rc)
	{
		cout << "Error executing SQLite3 query: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		string username, password, email;
		//init the users
		for (int i = 1; i <= rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				int pos = (i * cols) + j;

				if (j % 3 == 0)
				{
					username = resaults[pos];
				}
				else if (j % 3 == 1)
				{
					password = resaults[pos];
				}
				else
				{
					email = resaults[pos];
					this->_users.push_back(tempUser(username, password, email));
				}
			}
		}
	}

	rc = sqlite3_get_table(this->_db, "SELECT * FROM t_questions;", &resaults, &rows, &cols, &zErrMsg);

	if (rc)
	{
		cout << "Error executing SQLite3 query: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		int id;
		string question, correctAns, answer2, answer3, answer4;
		//init the questions
		for (int i = 1; i <= rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				int pos = (i * cols) + j;

				if (j % 6 == 0)
				{
					id = std::stoi(resaults[pos]);
				}
				else if (j % 6 == 1)
				{
					question = resaults[pos];
				}
				else if (j % 6 == 2)
				{
					correctAns = resaults[pos];
				}
				else if (j % 6 == 3)
				{
					answer2 = resaults[pos];
				}
				else if (j % 6 == 4)
				{
					answer3 = resaults[pos];
				}
				else
				{
					answer4 = resaults[pos];
					this->_questions.push_back(Question(id, question, correctAns, answer2, answer3, answer4));
				}
			}
		}
	}
}
/*
this function find if a user is exists in the database
input: the username string
output: true if the user exist otherwise false
*/
bool DataBase::isUserExists(string username)
{
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i]._name == username)
		{
			return true;
		}
	}
	return false;
}
/*
this function adds new user to the data base
input: the strings of the username, password and the email
output: true if the user added otherwise false 
*/
bool DataBase::addNewUser(string username, string password, string email)
{
	if (!this->isUserExists(username))
	{
		int rc = 0;
		char* zErrMsg = nullptr;
		string insert = "INSERT INTO t_users (username,password,email) VALUES(\"" + username + "\",\"" + password + "\",\"" + email + "\""  + ");";
		rc = sqlite3_exec(this->_db, insert.c_str(), NULL, NULL, &zErrMsg);//insert the user
		if (rc)
		{
			cout << "Error executing SQLite3 statement: " << sqlite3_errmsg(this->_db) << endl << endl;
			sqlite3_free(zErrMsg);
			return false;
		}

		this->_users.push_back(tempUser(username, password, email));
		return true;
	}

	return false;
}
/*
this function match between username and password
input: username and password
output: true if the password and the username match
*/
bool DataBase::isUserAndPassMatch(string username, string password)
{
	for (auto it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it)._name == username)
		{
			if ((*it)._password == std::to_string(hash<string>{}(password)))
			{
				return true;
			}
		}
	}

	return false;
}
/*
this function init the questions of the game
input: number of the questions
output: vector of questions
*/
vector<Question*> DataBase::initQuestions(int questionsNo)
{
	srand(time(NULL));
	int curr = rand() % questionsNo;
	vector<Question*> questions;
	vector<int> used;
	questions.push_back(&this->_questions.at(curr));
	used.push_back(curr);

	while (used.size() < questionsNo)
	{
		curr = rand() % questionsNo;
		if (find(used.begin(), used.end(), curr) == used.end())
		{
			questions.push_back(&this->_questions.at(curr));
			used.push_back(curr);
		}
	}

	return questions;
}
/*
this function insert new game to the db
input: none
output: the number of games
*/
int DataBase::insertNewGame() //need to check!
{
	int rc = 0;
	char* zErrMsg = nullptr;
	this->_gameCtr++;
	string insert = "INSERT INTO t_games (game_id,status,start_time,end_time) VALUES(" + std::to_string(this->_gameCtr) + ",0,0,0);" ;
	
	rc = sqlite3_exec(this->_db, insert.c_str(), NULL, NULL, &zErrMsg);

	if (rc)
	{
		string s =  sqlite3_errmsg(this->_db);
		sqlite3_free(zErrMsg);
		return -1;
	}

	return this->_gameCtr ;
}
/*
this function update the game status 
input: the game id
output: true if the game updated otherwise false
*/
bool DataBase::updateGameStatus(int gameId) 
{
	int rc = 0;
	char* zErrMsg = nullptr;
	string update = "UPDATE t_games SET status = 1, end_time = \"now\" WHERE game_id = " + std::to_string(gameId) + ";";
	rc = sqlite3_exec(this->_db, update.c_str(), NULL, NULL, &zErrMsg);

	if (rc)
	{
		cout << "Error executing SQLite3 statement: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
		return false;
	}

	return true;
}
/*
this function adds answer to the db
input: the game id the username the question id the answer the correction and the time
output: true if the question were add otherwise false
*/
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	int rc = 0;
	char* zErrMsg = nullptr;
	string IsCorrect = "";

	if (isCorrect)
	{
		IsCorrect = "1";
	}
	else
	{
		IsCorrect = "0";
	}

	string insert = "INSERT INTO t_players_answers (game_id,username,question_id,player_answer,is_correct,answer_time) values(" + std::to_string(gameId) + ",'" + username + "'," + std::to_string(questionId) + ",'" + answer + "'," + IsCorrect + "," + std::to_string(answerTime) + ");";
	rc = sqlite3_exec(this->_db, insert.c_str(), NULL, NULL, &zErrMsg);

	if (rc)
	{
		cout << "Error executing SQLite3 statement: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
		return false;
	}

	return true;
}
/*
this function return the best user scores
input: none
output: a vector of pairs with the username and the score
*/
vector<std::pair<string, int>> DataBase::getBestScores()
{
	int rc = 0;
	char* zErrMsg = nullptr;
	string select = "";
	vector<std::pair<string, int>> resaults;
	vector<std::pair<string, int>> top3;
	
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		select = "SELECT COUNT(is_correct) FROM t_players_answers WHERE is_correct = 1 AND username = '" + this->_users[i]._name + "';";
		rc = sqlite3_exec(this->_db, select.c_str(), callbackBestScores, NULL, &zErrMsg);

		resaults.push_back(std::pair<string,int>(this->_users[i]._name, currCount));

		if (rc)
		{
			cout << "Error executing SQLite3 statement: " << sqlite3_errmsg(this->_db) << endl << endl;
			sqlite3_free(zErrMsg);
			return resaults;
		}
	}

	int currMax = 0;
	int maxIndex = 0;

	for (int i = 0; i < 3; i++)
	{
		for (unsigned int j = 0; j < resaults.size(); j++)
		{
			if (resaults[j].second > currMax)
			{
				currMax = resaults[j].second;
				maxIndex = j;
			}
		}

		top3.push_back(resaults[maxIndex]);
		resaults.erase(resaults.begin() + maxIndex);
		currMax = 0;
	}

	return top3;
}
/*
this functions returns the personal status of a user
input: username
output: vector of the user stats
*/
vector<string> DataBase::getPersonalStatus(string username) 
{
	int rc = 0;
	char* zErrMsg = nullptr;
	string select = "";
	vector<string> data;

	select = "SELECT COUNT(username) FROM t_players_answers WHERE username = '" + username + "';";
	rc = sqlite3_exec(this->_db, select.c_str(), callbackCount, NULL, &zErrMsg);
	data.push_back(std::to_string(gamesCuont));

	select = "SELECT COUNT(is_correct) FROM t_players_answers WHERE username = '" + username + "' AND is_correct = 1;";
	rc = sqlite3_exec(this->_db, select.c_str(), callbackCount, NULL, &zErrMsg);
	data.push_back(std::to_string(gamesCuont));
	int correctAnss = gamesCuont;

	select = "SELECT COUNT(is_correct) FROM t_players_answers WHERE username = '" + username + "'";
	rc = sqlite3_exec(this->_db, select.c_str(), callbackCount, NULL, &zErrMsg);
	data.push_back(std::to_string(gamesCuont - correctAnss));

	select = "SELECT AVG(answer_time) FROM t_players_answers WHERE username = '" + username + "'";
	rc = sqlite3_exec(this->_db, select.c_str(), callbackPersonalStatus, NULL, &zErrMsg);
	avgTime.erase(std::remove(avgTime.begin(), avgTime.end(), '.'), avgTime.end());
	data.push_back(avgTime.substr(0, 4));

	return data;
}
/*
call back function
*/
int DataBase::callbackBestScores(void* notUsed, int argc, char** argv, char** azCol)
{
	currCount = std::stoi(argv[0]);
	return 0;
}
/*
call back function
*/
int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol)
{
	gamesCuont = std::stoi(argv[0]);
	return 0;
}
/*
call back function
*/
int DataBase::callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	avgTime = argv[0];
	return 0;
}
/*
call back function
*/
int DataBase::getGameCtr()
{
	return this->_gameCtr;
}
/*
this function adds a question to the db
input: the question and 4 answers
output: true if the question were add
*/
bool DataBase::addQuestionToDB(string question, string correctAns, string ans1, string ans2, string ans3)
{
	int rc = 0;
	char* zErrMsg = nullptr;

	string insert = "insert into t_questions(question, correct_ans,ans2,ans3,ans4) values('" + question + "','" + correctAns + "','" + ans1 + "','" +ans2 + "','" + ans3 + "');";
	rc = sqlite3_exec(this->_db, insert.c_str(), NULL, NULL, &zErrMsg);

	if (rc)
	{
		cout << "Error executing SQLite3 statement: " << sqlite3_errmsg(this->_db) << endl << endl;
		sqlite3_free(zErrMsg);
		return false;
	}

	return true;
}