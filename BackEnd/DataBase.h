#pragma once
#include "stadfx.h"
#include "tempUser.h"
#include "sqlite3.h"
#include "Question.h"

class DataBase
{
public:
	DataBase();
	DataBase(DataBase& other);
	void fillDataFromSQLServer();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionsNo);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	vector<std::pair<string, int>> getBestScores();
	vector<string> getPersonalStatus(string username);
	int getGameCtr();
	bool addQuestionToDB(string question, string correctAns, string ans1, string ans2, string ans3);
	~DataBase();
private:
	static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);
	vector<tempUser> _users;
	vector<Question> _questions;
	sqlite3* _db;
	int _gameCtr;
};

