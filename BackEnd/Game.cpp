#include "Game.h"

/*
this function is a game constructor
input: vector of the users, the number of questions, a db instance
output: none
*/
Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	this->_db = db;
	this->_questionNo = questionsNo;
	int status = this->_db.insertNewGame();
	this->_id = this->_db.getGameCtr();

	if (status == -1)
	{
		throw;
	}

	this->_questions = this->_db.initQuestions(this->_questionNo);
	this->_players = players;

	for (int i = 0; i < this->_players.size(); i++)
	{
		this->_results.insert(std::pair<string,int>(this->_players[i]->getUsername(), 0));
	}

	for (auto it = this->_players.begin(); it != this->_players.end(); it++)
	{
		(*it)->setGame(this);
	}
}
/*
this function is a distructor
input: none
output: none
*/
Game::~Game()
{
	this->_players.clear();
	this->_questions.clear();
}
/*
this function sends question to all the users in the game
input: none
output: none
*/
void Game::sendQuestionToAllUsers()
{
	int questionsCount = this->_questions.size();

	int question = this->_currQuestionIndex;
	string toSend = std::to_string(SEND_QUESTIONS_RESPONSE) + Helper::getPaddedNumber(this->_questions[question]->getQuestion().length(), 3) + this->_questions[question]->getQuestion();
	string* answers = this->_questions[question]->getAnswers();


 	for (int i = 0; i < 4; i++)
	{
		toSend += Helper::getPaddedNumber(answers[i].length(), 3);
		toSend += answers[i];
	}

	int correctAns = this->_questions[question]->getCorrectAnswerIndex() + 1;
	toSend += std::to_string(correctAns);

	this->_currentTurnAnswers = 0;

	for (std::vector<User*>::size_type i = 0; i != this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(toSend);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
	}
	
}
/*
this function handles the end of the game
input: none
output: none
*/
void Game::handleFinishGame()
{
	this->_db.updateGameStatus(this->_id); 
	string toSend = std::to_string(LEAVE_GAME_RESPONSE) + std::to_string(this->_players.size());

	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		toSend += Helper::getPaddedNumber(this->_players[i]->getUsername().length(), 2);
		toSend += this->_players[i]->getUsername();
		toSend += "score";
		toSend += Helper::getPaddedNumber(this->_results.find(this->_players[i]->getUsername())->second, 2);
	}

	for (std::vector<User*>::size_type i = 0; i != this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(toSend);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
	}
}
/*
this fucntion handles the next turn
input: none
output: true if the game still alive otherwise false
*/
bool Game::handleNextTurn()
{
	if (this->_players.size() == 0)
	{
		this->handleFinishGame();
		return false;
	}

	if (this->_currentTurnAnswers == this->_players.size())
	{
		if (this->_currQuestionIndex == this->_questions.size() - 1)
		{
			this->handleFinishGame();
			return false;
		}

		this->_currQuestionIndex++;
		this->sendQuestionToAllUsers();
	}

	return true;
}
/*
this function handles the answer from the user and check it 
input: the user, the answer index and the time
*/
bool Game::handleAnswerFromUser(User* user, int answerNo, int time) 
{
	this->_currentTurnAnswers++;
	bool correctAns = false;

	if (answerNo - 1 == this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex())
	{
		correctAns = true;
		auto it = this->_results.find(user->getUsername());
		it->second++;
	}

	if (answerNo == 5)
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(), "", correctAns, time);
	}
	else
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(), this->_questions[this->_currQuestionIndex]->getAnswers()[answerNo - 1], correctAns, time);
	}

	return this->handleNextTurn();
}
/*
id getter
*/
int Game::getID()
{
	return this->_id;
}
/*
this function handles the leave game option
input: the user
output: true if the user left the game otherwise false
*/
bool Game::leaveGame(User* currUser)
{
	auto it = find(this->_players.begin(), this->_players.end(), currUser);

	if (it != this->_players.end())
	{
		this->_players.erase(it);
		this->handleNextTurn();
		return true;
	}

	return false;
}
/*
this function insert the game to the db
input: none
output: true if the game were added otherwise false
*/
bool Game::insertGameToDB()
{
	this->_db.insertNewGame();
	return false;
}

