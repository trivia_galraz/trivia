#include "Question.h"

/*
this function is an empty constructor
input: none
output: none
*/
Question::Question()
{
}
/*
this function is question constructor 
input: question id, the question and 4 answers
*/
Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	this->_id = id;
	this->_question = question;
	this->_correctIndex = 0;
	this->_answers[0] = correctAnswer;
	this->_answers[1] = answer2;
	this->_answers[2] = answer3;
	this->_answers[3] = answer4;
}
//getters
string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctIndex;
}

int Question::getId()
{
	return this->_id;
}