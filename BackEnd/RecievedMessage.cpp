#include "RecievedMessage.h"

/*
this function is a message constructor
input: socket and message code
output: none
*/
RecievedMessage::RecievedMessage(SOCKET socket, int messageCode)
{
	this->_socket = socket;
	this->_messageCode = messageCode;
}
/*
this function is a message constructor
input: socket, message code and a vector of values
output: none
*/
RecievedMessage::RecievedMessage(SOCKET socket, int messageCode, vector<string> values)
{
	this->_socket = socket;
	this->_messageCode = messageCode;
	this->_values = values;
}
/*

this function is a message distructor
input: none
output: none
*/
RecievedMessage::~RecievedMessage()
{
	if (this->_user != nullptr)
	{
		this->_user->~User();
	}
	this->_values.clear();
}
//getters and setters
SOCKET RecievedMessage::getSock()
{
	return this->_socket;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}