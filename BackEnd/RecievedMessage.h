#pragma once
#include "stadfx.h"
#include "User.h"
class RecievedMessage
{
private:
	SOCKET _socket;
	User* _user;
	int _messageCode;
	vector<string> _values;
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);
	~RecievedMessage();
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();
};

