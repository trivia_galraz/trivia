#include "Room.h"
#include "User.h"

/*
this function is a room empty constructor
input: none
output: none
*/
Room::Room()
{
}
/*
this function is a room distructor
input: none
output: none
*/
Room::~Room()
{
}
/*
this function is a room constructor
input: room id, admin user, room name, max users, number of question and the time per question
output: none
*/
Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNo = questionNo;
	this->_questionTime = questionTime;
	this->_users.insert(this->_users.begin(), this->_admin);
}
/*
this function create user list message
input: vector of user, the exclude user
output: string of the users list
*/
string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{
	string toReturn = "";

	for (std::vector<User*>::size_type i = 0; i != this->_users.size(); i++)
	{
		if (_users[i] == excludeUser)
		{
			break;
		}

		toReturn += Helper::getPaddedNumber(this->_users[i]->getUsername().length(), 2);
		toReturn += this->_users[i]->getUsername();
	}

	return toReturn;
}
/*
this function sends a message to all of the users in the room
*/
void Room::sendMessage(User* excludeUser, string message)
{
	for (std::vector<User*>::size_type i = 0; i != this->_users.size(); i++)
	{
		if (_users[i] == excludeUser)
		{
			break;
		}

		this->_users[i]->send(message);
	}
}
/*
this function add a user to the room
*/
bool Room::joinRoom(User* user)
{
	if (this->_users.size() == this->_maxUsers)
	{
		cout << "Room is full" << endl;
		user->send(std::to_string(JOIN_ROOM_RESPONSE) + "1");
		return false;
	}
	user->send(std::to_string(JOIN_ROOM_RESPONSE) + "0" + Helper::getPaddedNumber(this->getQuistionNo(), 2) + Helper::getPaddedNumber(this->getQuestionTime(), 2));
	this->_users.push_back(user);
	for (int i = 0; i < this->_users.size(); i++)
	{
		this->_users[i]->send(this->getUsersListMessage());
	}
	return true;
}
/*
this function pulls out a user out of the room 
input: user
output: none
*/
void Room::leaveRoom(User* user) 
{
	unsigned int index = find(this->_users.begin(), this->_users.end(), user) - this->_users.begin();
	if (index <= this->_users.size())
	{
		this->_users.at(index)->send(std::to_string(LEAVE_ROOM_RESPONSE) + "0");
		this->_users.erase(this->_users.begin() + index);
		this->sendMessage(user,this->getUsersListMessage());
	}
}
/*
this function closes a room
input: the admin user
output: the room id if successed otherwise -1
*/
int Room::closeRoom(User* user)
{
	if (user != this->_admin)
	{
		return -1;
	}

	for (std::vector<User*>::size_type i = 0; i != this->_users.size(); i++)
	{
		this->_users[i]->send(std::to_string(CLOSE_ROOM_RESPONSE));
		if (this->_users[i]->getUsername() != this->_admin->getUsername())
		{
			this->_users[i]->clearRoom();
		}
		
	}

	return this->_id;
}
//getters and setters
vector<User*> Room::getUsers()
{
	return this->_users;
}

string Room::getUsersListMessage()
{
	string s = std::to_string(ROOM_USERS_RESPONSE) + std::to_string(this->_users.size()) + this->getUsersAsString(this->_users, nullptr);
	return s;
}

int Room::getQuistionNo()
{
	return this->_questionNo;
}

int Room::getQuestionTime()
{
	return this->_questionTime;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}