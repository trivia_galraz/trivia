#include "TriviaServer.h"

/*
*create a new socket
*input: none
*output: none
*/
TriviaServer::TriviaServer()
{
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{
		throw exception("Error can't create a socket");
	}
}
/*
*close the socket
*input: none
*output: none
*/
TriviaServer::~TriviaServer()
{
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}
/*
*this function is the main thread of the server
*input: none
*output: none
*/
void TriviaServer::server()
{
	thread* handle = new thread(&TriviaServer::handleRecievedMessages, this);
	try
	{
		bindAndListen();
	}
	catch (const exception& e)
	{
		throw e;
	}
	std::cout << "Listening to new clients" << std::endl;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		try
		{
			accept();

		}
		catch (const exception& e)
		{
			throw e;
		}

	}
}
/*
*binding and listening 
*input: none
*output: none
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(this->_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw exception("Error binding the socket");
	}
	// Start listening for incoming requests of clients
	if (::listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw exception("Error listening to the socket");
	}
}

/*
*accept the user and creating a new user socket
*input: none
*output: none
*/
void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(this->_socket, NULL, NULL);
	std::cout << "new client conected" << std::endl;
	if (clientSocket == INVALID_SOCKET)
	{
		throw exception("Error accepting the user");
	}
	// the function that handle the conversation with the client
	thread* handle = new thread(&TriviaServer::clientHandler, this, clientSocket);
	this->_handlers.push_back(handle);
}

/*
this function is the communication between the server and the client
the function build and add message to the messages queue
input: the socket of the client
output: none
*/
void TriviaServer::clientHandler(SOCKET socket)
{
	try
	{
		int typeCode = Helper::getMessageTypeCode(socket);
		while (typeCode != LEAVE_APP_REQUEST && typeCode != 0)
		{
			addRecievedMessages(buildRecieveMessage(socket, typeCode));
			cout << "new message added to the queue" << endl;
			this->_queueEmpty.notify_one();
			typeCode = Helper::getMessageTypeCode(socket);
		}
	}
	catch (const std::exception& e)
	{
		cout << "sorry somthing went wrong" << endl;
		cout << e.what() << endl;
		addRecievedMessages(buildRecieveMessage(socket, LEAVE_APP_REQUEST));
	}
	cout << "close socket" << endl;
	closesocket(socket); //closing the socket
	
}
/*
this function handle the recieved messages queue
the function allways runing in the back and awake when new
messages enters the queue
input: none
output: none
*/
void TriviaServer::handleRecievedMessages()
{
	User* user;
	RecievedMessage* message;
	try
	{
		std::unique_lock<std::mutex> lk(this->_mtxRecivedMessages);
		while (true)
		{
			this->_queueEmpty.wait(lk);//wait untill messages will enter to the queue
			while (!this->_queRcvMessages.empty())
			{
				message = this->_queRcvMessages.front();
				this->_queRcvMessages.pop();
				std::cout << "data for message " + std::to_string(message->getMessageCode()) << std::endl;
				switch (message->getMessageCode()) //switch the type code and handle 
				{
				case SIGN_IN_REQUEST:
					user = handleSignin(message);
					if (user != nullptr)
					{
						this->_connectedUsers[message->getSock()] = user;
					}
					break;
				case SIGN_OUT_REQUEST:
					handleSignout(message);
					break;
				case SIGN_UP_REQUEST:
					handleSignup(message);
					break;
				case CREATE_ROOM_REQUEST:
					handleCreateRoom(message);
					break;
				case CLOSE_ROOM_REQUEST:
					handleCloseRoom(message);
					break;
				case LIVE_ROOMS_REQUEST:
					handleGetRooms(message);
					break;
				case ROOM_USERS_REQUEST:
					handleGetUsersIn(message);
					break;
				case JOIN_ROOM_REQUEST:
					handleJoinRoom(message);
					break;
				case LEAVE_ROOM_REQUEST:
					handleLeaveRoom(message);
					break;
				case SEND_QUESTIONS_REQUEST:
					handleStartGame(message);
					break;
				case LEAVE_GAME_REQUEST:
					handleLeaveGame(message);
					break;
				case LEAVE_APP_REQUEST:
					handleSignout(message);
					::closesocket(message->getSock());
					break;
				case BEST_SCORES_REQUEST:
					handleGetBestScores(message);
					break;
				case CHECK_ANSWER_REQUEST:
					handlePlayerAnswer(message);
					break;
				case SELF_MODE_REQUEST:
					handleGetPersonalStatus(message);
					break;
				case ADD_QUESTION_REQUEST:
					handleAddQuestion(message);
					break;
				default:
					cout << "request not supported yet." << endl;
					break;
				}
			}
		}

	}
	catch (exception& e)
	{
		cout << "sorry somthing went wrong" << endl;
		cout << e.what() << endl;
	}
}
/*
this function adding messages to the queue
the function locks the queue mutex and add the message
input: message
output: none
*/
void TriviaServer::addRecievedMessages(RecievedMessage * message)
{
	this->_mtxRecivedMessages.lock();
	this->_queRcvMessages.push(message);
	this->_mtxRecivedMessages.unlock();
}
/*
this function build recive message using the protocol
the type code and the socket
input: socket and type code
output: new Recived message
*/
RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET socket, int typeCode)
{
	RecievedMessage* message;
	vector<string>* data = new vector<string>();
	string s;
	switch (typeCode)
	{
	case SIGN_IN_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case SIGN_UP_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case ROOM_USERS_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 4);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	case JOIN_ROOM_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 4);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	case CREATE_ROOM_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 1);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	case CHECK_ANSWER_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 1);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	case BEST_SCORES_REQUEST:
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	case ADD_QUESTION_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		message->setUser(this->_connectedUsers[socket]);
		break;
	default:
		message = new RecievedMessage(socket, typeCode);
		message->setUser(this->_connectedUsers[socket]);
		break;
	}
	return message;
}
/*
this function delete a user from the connected users
input: user's message
output: none
*/
void TriviaServer::safeDeleteUser(RecievedMessage * msg)
{
	try
	{
		this->handleSignout(msg);
		::closesocket(_socket);
	}
	catch (...)
	{

	}
}
/*
this function handles sign in case
input: the sign in message
output: the new user
*/
User * TriviaServer::handleSignin(RecievedMessage * msg)
{
	if (!this->_db.isUserAndPassMatch(msg->getValues()[0], msg->getValues()[1]))
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_IN_RESPONSE) + "1");
		cout << "Wrong username or pasword" << endl;
		return nullptr;
	}

	if (this->getUserByName(msg->getValues()[0]) != nullptr)
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_IN_RESPONSE) +"2");
		cout << "user is allready connected" << endl;
		return nullptr;
	}

	User* user = new User(msg->getValues()[0], msg->getSock());
	user->send(std::to_string(SIGN_IN_RESPONSE) + "0");
	cout << user->getUsername() + " signed in" << endl;
	return user;
}
/*
this function handles the sign up case
input: the sign up message 
output: true if the user signed, otherwise false
*/
bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	if (!Validator::isPasswordValid(password))
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_UP_RESPONSE) + "1");
		cout << "password is invalid" << endl;
		return false;
	}

	if (!Validator::isUaernameValid(username))
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_UP_RESPONSE) + "3");
		cout << "username is invaild" << endl;
		return false;
	}

	if (this->_db.isUserExists(username))
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_UP_RESPONSE) + "2");
		cout << "user is allready singed up" << endl;
		return false;
	}

	bool addStatus = this->_db.addNewUser(username,std::to_string(hash<string>{}(password)), email);
	if (!addStatus)
	{
		Helper::sendData(msg->getSock(), std::to_string(SIGN_UP_RESPONSE) +"4");
		cout << "something went wrong while adding user to database" << endl;
		return false;
	}

	Helper::sendData(msg->getSock(), std::to_string(SIGN_UP_RESPONSE) + "0");
	cout << "new user " << username << " signed up" << endl;
	return true;
}
/*
this function handle sign out case
input: the sign out message
output: null
*/
void TriviaServer::handleSignout(RecievedMessage * msg)
{
	if (msg->getUser()!=nullptr)
	{
		this->handleCloseRoom(msg);
		this->handleLeaveRoom(msg);
		this->handleLeaveGame(msg);
		cout << msg->getUser()->getUsername() << " signed out" << endl;
		this->_connectedUsers.erase(msg->getSock());
	}
}
/*
this function end the game for a player
input : leave game message 
output : none
*/
void TriviaServer::handleLeaveGame(RecievedMessage *msg)
{
	if (msg->getUser()->leaveGame())
	{
		delete msg->getUser()->getGame();
	}
}
/*
this function handels the start of a game
input: the recieved message
output: none
*/
void TriviaServer::handleStartGame(RecievedMessage * msg)
{
	try
	{
		Room* room = msg->getUser()->getRoom();
		Game* game = new Game(room->getUsers(), room->getQuistionNo(), this->_db);
		for (unsigned int i = 0; i < room->getUsers().size(); i++)
		{
			room->getUsers()[i]->clearRoom();
		}
		this->_roomCount--;
		this->_roomsList.erase(room->getId());
		room->~Room();
		game->sendQuestionToAllUsers();
	}
	catch (const std::exception&)
	{
		msg->getUser()->send(std::to_string(SEND_QUESTIONS_RESPONSE) + "0");
	}
}
/*
this function handle the player answer
input: the player message
output: none
*/
void TriviaServer::handlePlayerAnswer(RecievedMessage *msg)
{
	if (msg->getUser()->getGame() == nullptr)
	{
		return;
	}
	msg->getUser()->getGame()->handleAnswerFromUser(msg->getUser(), stoi(msg->getValues()[0]), stoi(msg->getValues()[1]));
}
/*
this function create a room
input: message
output: true if the room was create otherwise false
*/
bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	if (user == nullptr)
	{
		return false;
	}
	this->_roomCount++;
	user->createRoom(_roomCount, msg->getValues()[0], stoi(msg->getValues()[1]), stoi(msg->getValues()[2]), stoi(msg->getValues()[3]));
	this->_roomsList[this->_roomCount] = user->getRoom();
	cout << user->getUsername() << " created a room" << endl;
	return true;
}
/*
this function close a room
input: the close room message
output: true if the room closed otherwise false
*/
bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	if (msg->getUser()->getRoom() == nullptr)
	{
		return false;
	}
	this->_roomCount--;
	this->_roomsList.erase(msg->getUser()->closeRoom());
	return true;
}
/*
this function handle join room request
input: msg
output: true if the user joined the room otherwise false
*/
bool TriviaServer::handleJoinRoom(RecievedMessage * msg)
{
	User* user = msg->getUser();
	if (user == nullptr)
	{
		return false;
	}
	Room* room = this->getRoomById(stoi(msg->getValues()[0]));
	if (room == nullptr)
	{
		user->send(std::to_string(JOIN_ROOM_RESPONSE) + "2");
		cout << "There is no room with this id" << endl;
		return false;
	}
	user->joinRoom(room);
	return true;
}
/*
this function leave the room and notice all the users
input: the leave room message
output: true if the user left the room otherwise false
*/
bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	if (msg->getUser()->getRoom() == nullptr)
	{
		return false;
	}
	msg->getUser()->leaveRoom();
	return true;
}
/*
this function send to the user all the users in the room
input: message
output: none
*/
void TriviaServer::handleGetUsersIn(RecievedMessage * msg)
{
	Room* room = this->getRoomById(stoi(msg->getValues()[0]));
	if (room == nullptr)
	{
		msg->getUser()->send(std::to_string(ROOM_USERS_RESPONSE) + "0");
		return;
	}
	msg->getUser()->send(room->getUsersListMessage());
}
/*
this function send to the user a list of the rooms
input: message from the user
output: none
*/
void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	string s = std::to_string(LIVE_ROOMS_RESPONSE);
	s += Helper::getPaddedNumber(this->_roomCount, 2);
	for (unsigned int i = 1; i <= this->_roomCount; i++)
	{
		s += Helper::getPaddedNumber(this->_roomsList[i]->getId(),2);
		s += Helper::getPaddedNumber(this->_roomsList[i]->getName().length(), 2);
		s += this->_roomsList[i]->getName();
	}
	msg->getUser()->send(s);
}
/*
this function returns the 3 users that achieve the highest scores
input : message
output : none
*/
void TriviaServer::handleGetBestScores(RecievedMessage *msg)
{
	vector<std::pair<string, int>> top3 = this->_db.getBestScores();
	string s = std::to_string(BEST_SCORES_RESPONSE);

	for (int i = 0; i < 3; i++)
	{
		s += Helper::getPaddedNumber(top3[i].first.length(), 2);
		s += top3[i].first;
		s += std::to_string(std::to_string(top3[i].second).length());
		s += std::to_string(top3[i].second);
	}

	msg->getUser()->send(s);
}
/*
this function handles the personal status request
input: the recieved message
output: none
*/
void TriviaServer::handleGetPersonalStatus(RecievedMessage *msg)
{
	vector<string> data = this->_db.getPersonalStatus(msg->getUser()->getUsername());
	string s = std::to_string(SELF_MODE_RESPONSE);

	s += Helper::getPaddedNumber(stoi(data[0]), 4);
	s += Helper::getPaddedNumber(stoi(data[1]), 6);
	s += Helper::getPaddedNumber(stoi(data[2]), 6);
	s += Helper::getPaddedNumber(stoi(data[3]), 4);

	msg->getUser()->send(s);
}
/*
this function return a user 
from the connected users list by username
input: string of the user name
output: the user from the list
*/
User * TriviaServer::getUserByName(string username)
{
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		if (it->second->getUsername() == username)
		{
			return it->second;
		}
	}
	return nullptr;
}
/*
this function return a user
from the connected user list by the socket
input: user socket
output: the user
*/
User * TriviaServer::getUserBySocket(SOCKET socket)
{
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		if (it->first == socket)
		{
			return it->second;
		}
	}
	return nullptr;
}
/*
this functin return a room from the room list by his id
input: room id
output: the room
*/
Room * TriviaServer::getRoomById(int roomID)
{
	return this->_roomsList[roomID];
}
void TriviaServer::handleAddQuestion(RecievedMessage* message)
{
	if (this->_db.addQuestionToDB(message->getValues()[0], message->getValues()[1], message->getValues()[2], message->getValues()[3], message->getValues()[4]))
	{
		message->getUser()->send(std::to_string(ADD_QUESTION_RESPONSE) + "0");
	}
	else
	{
		message->getUser()->send(std::to_string(ADD_QUESTION_RESPONSE) + "1");
	}
}