#include "User.h"
#include "Room.h"

/*
this function is a user empty constructor
input: socket and message code
output: none
*/
User::User()
{
}
/*
this function is a user distructor
input: socket and message code
output: none
*/
User::~User()
{
}
/*
this function is a User constructor
input: username and socket
output: none
*/
User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
}
/*
this function send a message to the user socket
input: the string of the message
output: none
*/
void User::send(string message)
{
	try 
	{
		Helper::sendData(this->_sock, message);
	}
	catch (exception& e)
	{
		cout << message << endl;
		cout << e.what() << endl;
	}

}
/*
this function creates a room
input: the room id, the room name, max users number, question number and the question time
output: none
*/
bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		Helper::sendData(this->_sock, std::to_string(CREATE_ROOM_RESPONSE) + "1");
		return false;
	}

	this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	Helper::sendData(this->_sock, std::to_string(CREATE_ROOM_RESPONSE) + std::to_string(roomId));

	return true;
}
/*
this function joined the user to a room
input: the room to join
output: true if the user joined otherwise false
*/
bool User::joinRoom(Room* room)
{
	if (this->_currRoom != nullptr)
	{
		return false;
	}

	bool success = room->joinRoom(this);

	if (success)
	{
		this->_currRoom = room;
		return true;
	}

	return false;
}
/*
this function pulls out a user from a room
input: none
output: none
*/
void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}
/*
this function close a room
input: none
output: if the room closed the room id otherwise false
*/
int User::closeRoom()
{
	if (this->_currRoom == nullptr)
	{
		return -1;
	}

	int roomId = this->_currRoom->closeRoom(this);
	this->_currRoom = nullptr;
	return roomId;
}
/*
this function pulls out a user from a game
input: none
output: true if the user left the room otherwise false
*/
bool User::leaveGame()
{
	if (this->_currGame != nullptr)
	{
		this->_currGame = nullptr;
		return true;
	}

	return false;
}
/*
this function is the compare between users operator
input: other user
output: true if the user if equal otherwise false
*/
bool User::operator==(User& other)
{
	if (this->_username == other.getUsername())
	{
		return true;
	}

	return false;
}
//setters and getters
string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* gm)
{
	this->_currRoom = nullptr;
	this->_currGame = gm;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}