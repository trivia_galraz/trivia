#pragma once
#include "stadfx.h"

class Room;
class Game;

class User
{
public:
	User();
	User(string username, SOCKET sock);
	~User();
	void send(string message);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);
	void clearRoom();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* room);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	bool operator==(User& other);

private:
	string _username;
	Room* _currRoom; 
	Game* _currGame;
	SOCKET _sock;
};


