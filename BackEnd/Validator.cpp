#include "Validator.h"

/*
this function valid a password
input: string of a password
output: true if the password valid otherwise false
*/
bool Validator::isPasswordValid(string password)
{
	bool aUpper = false;
	bool aLower = false;
	bool aDigit = false;
	
	if (password.length() < 3)
	{
		return false;
	}
	//looking for at least one digit, one upper- case letter and onelower- case letter.
	for (int i = 0; password[i]; ++i)
	{
		if (isupper(password[i]))
		{
			aUpper = true;
		}
		else if (islower(password[i]))
		{
			aLower = true;
		}
		else if (isdigit(password[i]))
		{
			aDigit = true;
		}
	}

	if (aUpper && aLower && aDigit && password.find(' ') == string::npos)
	{
		return true;
	}

	return false;
}
/*
this function valid a username
input: string of a username
output: true if the username valid otherwise false
*/
bool Validator::isUaernameValid(string username)
{
	if (username.length() < 3)
	{
		return false;
	}

	//checking if the first char is an alphabet
	if (!isalpha(username[0]))
	{
		return false;
	}

	//checking if the username contains a space
	if (username.find(' ') != string::npos)
	{
		return false;
	}

	return true;
}