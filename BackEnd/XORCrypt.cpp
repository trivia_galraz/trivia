#include "stadfx.h"
#include "XORCrypt.h"


XORCrypt::XORCrypt()
{
}
XORCrypt::~XORCrypt()
{
}
/*
this function is the encrypt function 
which is also the decrypt
input: string to encrypt/decrypt
output: the encrypted/decrypted string
*/
string XORCrypt::encryptDecrypt(string str)
{
	char key = ';';
	string output = str;

	for (int i = 0; i < str.size(); i++)
	{
		output[i] = str[i] ^ key;
	}
	return output;
}
