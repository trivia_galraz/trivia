﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class BestScores : Form
    {
        NetworkStream clientStream;

        public BestScores()
        {
            InitializeComponent();
        }

        public BestScores(NetworkStream clientStream)
        {
            this.clientStream = clientStream;
            InitializeComponent();
            GetWinners();
        }

        //this function requests the winners from the server, and updates the labels
        //according to the returned values.
        private void GetWinners()
        {
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";

            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("223"));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                bufferIn = new byte[4096];
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            string[] text = new string[3];

            for (int i = 0, j = 3; i < 3; i++) //parsing the input from the server to 3 strings of the name of the winners.
            {
                text[i] = input.Substring(j + 2, int.Parse(input.Substring(j, 2)));
                text[i] += ": " + int.Parse(input.Substring(j + 3 + text[i].Length, int.Parse(input.Substring(j + 2 + text[i].Length, 1)))).ToString();
                j += int.Parse(input.Substring(j, 2)) + int.Parse(input.Substring(j + 2 + int.Parse(input.Substring(j, 2)), 1)) + 3;
            }
    
            //updating the labels.
            this.label1.Text = text[0];
            this.label2.Text = text[1];
            this.label3.Text = text[2];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
