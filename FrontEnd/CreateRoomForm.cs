﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class CreateRoomForm : Form
    {
        NetworkStream clientStream;
        bool name;
        bool players;
        bool questions;
        bool time;

        public CreateRoomForm()
        {
            InitializeComponent();
        }
        public CreateRoomForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void SendBtn_Click(object sender, EventArgs e)
        {
            //checking if all of the textBoxes are filled
            if(!(name && players && questions && time))
            {
                return;
            }
            string toSend = XORCrypt.EncryptDecrypt("213" + RoomNameTextBox.Text.Length.ToString("00") + RoomNameTextBox.Text + NumOfPlayersTextBox.Text + NumOfQuesitonsTextBox.Text.PadLeft(2, '0') + TimePerQuestionTextBox.Text.PadLeft(2, '0'));
            byte[] buffer = new ASCIIEncoding().GetBytes(toSend);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            byte[] bufferIn;
            int bytesRead = 0;
            string input;
            bufferIn = new byte[4];
            bytesRead = clientStream.Read(bufferIn, 0, 4);
            input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));

            //checking the servers input.
            if(input.Equals("1140"))
            {
                MessageBox.Show("Sorry the room did not create");
            }
            else
            {
                this.Hide();
                RoomGameForm form = new RoomGameForm(clientStream, RoomNameTextBox.Text, NumOfQuesitonsTextBox.Text, TimePerQuestionTextBox.Text, input.Substring(3, 1), true);
                form.ShowDialog();
                this.Close();
            }
        }

        //this fucntion if checking the input of the room name in real time.
        private void RoomNameTextBox_TextChanged(object sender, EventArgs e)
        {
            //checking length of the rooms name.
            if(((TextBox)sender).Text.Length < 3)
            {
                RoomNamePic.Image = Properties.Resources.invalid;
                this.name = false;
            }
            else
            {
                RoomNamePic.Image = Properties.Resources.valid;
                this.name = true;
            }
        }

        //this fucntion if checking the input of the number of players in real time.
        private void NumOfPlayersTextBox_TextChanged(object sender, EventArgs e)
        {
            int num;

            //chcking if the number of players input is a number.
            if(int.TryParse(((TextBox)sender).Text, out num))
            {
                //limiting the number of players to be 8 maximum.
                if(num>=1&&num<=8)
                {
                    pictureBox1.Image = Properties.Resources.valid;
                    this.players = true;
                }
                else
                {
                    pictureBox1.Image = Properties.Resources.invalid;
                    this.players = false;
                }
            }
            else
            {
                pictureBox1.Image = Properties.Resources.invalid;
                this.players = false;
            }
        }

        //this fucntion if checking the input of the number of questions in real time.
        private void NumOfQuesitonsTextBox_TextChanged(object sender, EventArgs e)
        {
            int num;

            //chcking if the number of questions input is a number.
            if (int.TryParse(((TextBox)sender).Text, out num))
            {
                //limiting the number of questions to be 20 maximum.
                if (num >= 1 && num <= 20)
                {
                    pictureBox3.Image = Properties.Resources.valid;
                    this.questions = true;
                }
                else
                {
                    pictureBox3.Image = Properties.Resources.invalid;
                    this.questions = false;
                }
            }
            else
            {
                pictureBox3.Image = Properties.Resources.invalid;
                this.questions = false;
            }
        }

        //this fucntion if checking the input of time per question in real time.
        private void TimePerQuestionTextBox_TextChanged(object sender, EventArgs e)
        {
            int num;

            //chcking if the number of questions input is a number.
            if (int.TryParse(((TextBox)sender).Text, out num))
            {
                //limiting the time per question to be 60 maximum.
                if (num >= 1 && num <= 60)
                {
                    pictureBox4.Image = Properties.Resources.valid;
                    this.time = true;
                }
                else
                {
                    pictureBox4.Image = Properties.Resources.invalid;
                    this.time = false;
                }
            }
            else
            {
                pictureBox4.Image = Properties.Resources.invalid;
                this.time = false;
            }
        }
    }
}
