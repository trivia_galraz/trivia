﻿namespace Frontend
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.JoinRoom = new System.Windows.Forms.Button();
            this.CreateRoom = new System.Windows.Forms.Button();
            this.MyStatus = new System.Windows.Forms.Button();
            this.BestScores = new System.Windows.Forms.Button();
            this.CurrUsernameLbl = new System.Windows.Forms.Label();
            this.SignOut = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // JoinRoom
            // 
            this.JoinRoom.BackColor = System.Drawing.SystemColors.Window;
            this.JoinRoom.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JoinRoom.Location = new System.Drawing.Point(434, 155);
            this.JoinRoom.Name = "JoinRoom";
            this.JoinRoom.Size = new System.Drawing.Size(162, 51);
            this.JoinRoom.TabIndex = 9;
            this.JoinRoom.Text = "Join Room";
            this.JoinRoom.UseVisualStyleBackColor = false;
            this.JoinRoom.Click += new System.EventHandler(this.JoinRoom_Click);
            // 
            // CreateRoom
            // 
            this.CreateRoom.BackColor = System.Drawing.SystemColors.Window;
            this.CreateRoom.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateRoom.Location = new System.Drawing.Point(434, 212);
            this.CreateRoom.Name = "CreateRoom";
            this.CreateRoom.Size = new System.Drawing.Size(162, 51);
            this.CreateRoom.TabIndex = 10;
            this.CreateRoom.Text = "Create Room";
            this.CreateRoom.UseVisualStyleBackColor = false;
            this.CreateRoom.Click += new System.EventHandler(this.CreateRoom_Click);
            // 
            // MyStatus
            // 
            this.MyStatus.BackColor = System.Drawing.SystemColors.Window;
            this.MyStatus.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyStatus.Location = new System.Drawing.Point(434, 269);
            this.MyStatus.Name = "MyStatus";
            this.MyStatus.Size = new System.Drawing.Size(162, 51);
            this.MyStatus.TabIndex = 11;
            this.MyStatus.Text = "My Status";
            this.MyStatus.UseVisualStyleBackColor = false;
            this.MyStatus.Click += new System.EventHandler(this.MyStatus_Click);
            // 
            // BestScores
            // 
            this.BestScores.BackColor = System.Drawing.SystemColors.Window;
            this.BestScores.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BestScores.Location = new System.Drawing.Point(434, 326);
            this.BestScores.Name = "BestScores";
            this.BestScores.Size = new System.Drawing.Size(162, 51);
            this.BestScores.TabIndex = 13;
            this.BestScores.Text = "Best Scores";
            this.BestScores.UseVisualStyleBackColor = false;
            this.BestScores.Click += new System.EventHandler(this.BestScores_Click);
            // 
            // CurrUsernameLbl
            // 
            this.CurrUsernameLbl.AutoSize = true;
            this.CurrUsernameLbl.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrUsernameLbl.Location = new System.Drawing.Point(13, 12);
            this.CurrUsernameLbl.Name = "CurrUsernameLbl";
            this.CurrUsernameLbl.Size = new System.Drawing.Size(0, 18);
            this.CurrUsernameLbl.TabIndex = 14;
            // 
            // SignOut
            // 
            this.SignOut.BackColor = System.Drawing.SystemColors.Window;
            this.SignOut.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignOut.ForeColor = System.Drawing.Color.Black;
            this.SignOut.Location = new System.Drawing.Point(434, 97);
            this.SignOut.Name = "SignOut";
            this.SignOut.Size = new System.Drawing.Size(162, 52);
            this.SignOut.TabIndex = 9;
            this.SignOut.Text = "SignOut";
            this.SignOut.UseVisualStyleBackColor = false;
            this.SignOut.Click += new System.EventHandler(this.SignOut_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(19, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Window;
            this.button1.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(888, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 31);
            this.button1.TabIndex = 6;
            this.button1.Text = "Bye";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.SignOut_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Window;
            this.button2.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(434, 383);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(162, 51);
            this.button2.TabIndex = 13;
            this.button2.Text = "Add Question";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(990, 441);
            this.Controls.Add(this.SignOut);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.JoinRoom);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CreateRoom);
            this.Controls.Add(this.CurrUsernameLbl);
            this.Controls.Add(this.MyStatus);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.BestScores);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trivia";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button JoinRoom;
        private System.Windows.Forms.Button CreateRoom;
        private System.Windows.Forms.Button MyStatus;
        private System.Windows.Forms.Button BestScores;
        private System.Windows.Forms.Label CurrUsernameLbl;
        private System.Windows.Forms.Button SignOut;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

