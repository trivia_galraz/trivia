﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class Menu : Form
    {
        NetworkStream clientStream;

        public Menu()
        {
            InitializeComponent();
        }

        public Menu(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("299"));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void SignOut_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("201"));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            SignIn sn = new SignIn(this.clientStream);
            this.Hide();
            sn.ShowDialog();
        }

        private void JoinRoom_Click(object sender, EventArgs e)
        {
            this.Hide();
            RoomForm rf = new RoomForm(this.clientStream);
            rf.ShowDialog();
            this.Show();
        }

        private void CreateRoom_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateRoomForm crf = new CreateRoomForm(this.clientStream);
            crf.ShowDialog();
            this.Show();
        }

        private void MyStatus_Click(object sender, EventArgs e)
        {
            this.Hide();
            MyStatus ms = new MyStatus(this.clientStream);
            ms.ShowDialog();
            this.Show();
        }

        private void BestScores_Click(object sender, EventArgs e)
        {
            this.Hide();
            BestScores bs = new BestScores(this.clientStream);
            bs.ShowDialog();
            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            QuestionAdd QA = new QuestionAdd(this.clientStream);
            QA.ShowDialog();
            this.Show();
        }
    }
}
