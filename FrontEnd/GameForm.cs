﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class GameForm : Form
    {
        RoomGameForm parent;
        NetworkStream clientStream;
        int correctAnswersCnt;
        int correctAns;
        int interval;
        int currTime;
        SoundPlayer clock;

        public GameForm()
        {
            InitializeComponent();
        }

        public GameForm(NetworkStream clientStream ,string[] questions, int correctAns, int interval, RoomGameForm parent)
        {
            InitializeComponent();

            this.parent = parent;
            this.clientStream = clientStream;
            this.correctAns = correctAns;
            this.interval = interval;

            //starting the timer
            this.timer1.Interval = 1000;
            this.timer1.Enabled = true;

            this.currTime = interval;
            this.Time.Text = interval.ToString();
            
            //updating the quesitons lables.
            this.label1.Text = questions[0];
            this.textBox1.Text = questions[1];
            this.textBox2.Text = questions[2];
            this.textBox3.Text = questions[3];
            this.textBox4.Text = questions[4];

            //starting the clock sound effect.
            this.clock = new SoundPlayer("clock.wav");
            this.clock.PlayLooping();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.clock.Stop();
            this.Close();
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            ChangeColors();
            Application.DoEvents();
            Thread.Sleep(1000);
            this.textBox1.BackColor = Color.Crimson;
            this.textBox2.BackColor = Color.Crimson;
            this.textBox3.BackColor = Color.Crimson;
            this.textBox4.BackColor = Color.Crimson;
            SendAnswer(1);
            this.currTime = this.interval;
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            ChangeColors();
            Application.DoEvents();
            Thread.Sleep(1000);
            this.textBox1.BackColor = Color.Crimson;
            this.textBox2.BackColor = Color.Crimson;
            this.textBox3.BackColor = Color.Crimson;
            this.textBox4.BackColor = Color.Crimson;
            SendAnswer(3);
            this.currTime = this.interval;
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            ChangeColors();
            Application.DoEvents();
            Thread.Sleep(1000);
            this.textBox1.BackColor = Color.Crimson;
            this.textBox2.BackColor = Color.Crimson;
            this.textBox3.BackColor = Color.Crimson;
            this.textBox4.BackColor = Color.Crimson;
            SendAnswer(2);
            this.currTime = this.interval;
        }

        private void textBox4_Click(object sender, EventArgs e)
        {
            ChangeColors();
            Application.DoEvents();
            Thread.Sleep(1000);
            this.textBox1.BackColor = Color.Crimson;
            this.textBox2.BackColor = Color.Crimson;
            this.textBox3.BackColor = Color.Crimson;
            this.textBox4.BackColor = Color.Crimson;
            SendAnswer(4);
            this.currTime = this.interval;
        }

        //this function is sending the players answer to the server.
        private void SendAnswer(int answerNum)
        {
            if(this.correctAns == answerNum) //checking if player's answer is correct.
            {
                this.clock.Stop();
                SoundPlayer sp = new SoundPlayer("correct.wav");
                sp.Play();
                Thread.Sleep(500);
                this.clock.PlayLooping();

                this.correctAnswersCnt++;
            }

            else if(answerNum != 5)
            {
                this.clock.Stop();
                SoundPlayer sp = new SoundPlayer("incorrect.wav");
                sp.Play();
                Thread.Sleep(500);
                this.clock.PlayLooping();
            }

            try
            {
                //sending the answer to server.
                string toSend = XORCrypt.EncryptDecrypt("219" + answerNum.ToString() + (this.interval - this.currTime).ToString());
                byte[] buffer = new ASCIIEncoding().GetBytes(toSend); 
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            GetNextQuestion();
        }

        //this function is getting the next question form the server.
        private void GetNextQuestion() 
        {
            try
            {
                byte[] bufferIn;
                int bytesRead = 0;
                string input = "";

                bufferIn = new byte[4096];
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));

                //checking servers input.
                if (input.StartsWith("118"))
                {
                    try
                    {
                        input = input.Replace(";", "");
                        string[] questions = GameQuestions(input.Substring(3)); 
                        this.label1.Text = questions[0];
                        this.textBox1.Text = questions[1];
                        this.textBox2.Text = questions[2];
                        this.textBox3.Text = questions[3];
                        this.textBox4.Text = questions[4];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return;
                    }
                }
                else if(input.StartsWith("121"))
                {
                    GetScores(input.Substring(3));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //this function is getting the scores of all of the players in the current room, and opens the form of the scores.
        void GetScores(string input)
        {          
            int len = 0;
            string currName = "";
            int currScore = 0;

            int playersCnt = int.Parse(input.Substring(0,1));
            List<KeyValuePair<string, int>> scores = new List<KeyValuePair<string, int>>();

            input = input.Substring(1);

            //parsing the servers input.
            for(int i = 0; i < playersCnt; i++)
            {
                if(i > 0)
                {
                    input = input.Substring(2);
                }
                len = int.Parse(input.Substring(0,2));
                input = input.Substring(2);
                currName = input.Substring(0,len);
                input = input.Substring(len + 5);
                currScore = int.Parse(input.Substring(0,2));
                scores.Add(new KeyValuePair<string, int>(currName, currScore));
            }

            this.clock.Stop();
            this.timer1.Stop();
            this.Hide();
            Application.DoEvents();

            //opening the scores form.
            Scores s = new Scores(scores);
            s.ShowDialog();
            this.Close();
        }

        //this funtion updates the textboxes colors according to the answer.
        private void ChangeColors()
        {
            switch (this.correctAns) //updating the colors of textboxes according to the correct answer.
            {
                case 1:
                    textBox1.BackColor = Color.Green;
                    textBox2.BackColor = Color.Red;
                    textBox3.BackColor = Color.Red;
                    textBox4.BackColor = Color.Red;
                    break;
                case 2:
                    textBox2.BackColor = Color.Green;
                    textBox1.BackColor = Color.Red;
                    textBox3.BackColor = Color.Red;
                    textBox4.BackColor = Color.Red;
                    break;
                case 3:
                    textBox3.BackColor = Color.Green;
                    textBox2.BackColor = Color.Red;
                    textBox1.BackColor = Color.Red;
                    textBox4.BackColor = Color.Red;
                    break;
                case 4:
                    textBox4.BackColor = Color.Green;
                    textBox1.BackColor = Color.Red;
                    textBox2.BackColor = Color.Red;
                    textBox3.BackColor = Color.Red;
                    break;
                default:
                    break;
                
            }
        }

        //this function is getting the question from the server and parsing it into an array of stirngs.
        private string[] GameQuestions(string input) 
        {
            this.correctAns = int.Parse(input.Substring(input.Length - 1));
            string[] data = new string[5];

            //parsing input
            for (int i = 0, j = 0; i < 5; i++)
            {
                data[i] = input.Substring(j + 3, int.Parse(input.Substring(j, 3)));
                j += 3 + int.Parse(input.Substring(j, 3));
            }
            MixQuestions();
            return data;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            //updating time to answer if the time isn't up.
            if(this.currTime > 0)
            {
                this.currTime--;
                this.Time.Text = currTime.ToString(); ;
            }
            else //if the time is up.
            {
                this.clock.Stop();
                //playing "time's up" sound.
                SoundPlayer sp = new SoundPlayer("timeup.wav");
                sp.Play();
                Thread.Sleep(500);
                this.clock.PlayLooping();

                this.timer1.Enabled = false;

                this.currTime = 0;
                SendAnswer(5);

                this.textBox1.BackColor = Color.Red;
                this.textBox2.BackColor = Color.Red;
                this.textBox3.BackColor = Color.Red;
                this.textBox4.BackColor = Color.Red;
                Application.DoEvents();
                this.textBox1.BackColor = Color.Crimson;
                this.textBox2.BackColor = Color.Crimson;
                this.textBox3.BackColor = Color.Crimson;
                this.textBox4.BackColor = Color.Crimson;

                Thread.Sleep(1500);
                this.Time.Text = "Time's Up!";

                this.Time.Text = this.interval.ToString();
                this.currTime = interval;
                this.timer1.Enabled = true;
            }
        }

        //this funtion is mixing the textboxes so they won't be the same every quesiton. 
        private void MixQuestions()
        {
            Point[] locations = new Point[4];
            //getting the locations of the textboxes into an array of points.
            locations[0] = textBox1.Location;
            locations[1] = textBox2.Location;
            locations[2] = textBox3.Location;
            locations[3] = textBox4.Location;

            locations = ShuffleArray(locations);

            //updating the new locations.
            textBox1.Location = locations[0];
            textBox2.Location = locations[1];
            textBox3.Location = locations[2];
            textBox4.Location = locations[3];

        }

        //this function is shuffling the array of points.
        private Point[] ShuffleArray(Point[] array)
        {
            Random r = new Random();
            for (int i = array.Length; i > 0; i--)
            {
                int j = r.Next(i);
                Point k = array[j];
                array[j] = array[i - 1];
                array[i - 1] = k;
            }

            return array;
        }
    }
}
