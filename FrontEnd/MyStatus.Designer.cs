﻿namespace Frontend
{
    partial class MyStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyStatus));
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gamesPlayedLbl = new System.Windows.Forms.Label();
            this.correctAnssLbl = new System.Windows.Forms.Label();
            this.wrongAnssLbl = new System.Windows.Forms.Label();
            this.avgTimePAns = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 30);
            this.label1.TabIndex = 4;
            this.label1.Text = "My performances:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(18, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 30);
            this.label5.TabIndex = 5;
            this.label5.Text = "Games played:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(18, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(261, 30);
            this.label6.TabIndex = 6;
            this.label6.Text = "Correct Answers:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(20, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(241, 30);
            this.label7.TabIndex = 7;
            this.label7.Text = "Wrong answers:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Teal;
            this.label8.Location = new System.Drawing.Point(18, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(383, 30);
            this.label8.TabIndex = 8;
            this.label8.Text = "Avarage time per answer:";
            // 
            // gamesPlayedLbl
            // 
            this.gamesPlayedLbl.AutoSize = true;
            this.gamesPlayedLbl.Location = new System.Drawing.Point(430, 118);
            this.gamesPlayedLbl.Name = "gamesPlayedLbl";
            this.gamesPlayedLbl.Size = new System.Drawing.Size(0, 30);
            this.gamesPlayedLbl.TabIndex = 9;
            // 
            // correctAnssLbl
            // 
            this.correctAnssLbl.AutoSize = true;
            this.correctAnssLbl.Location = new System.Drawing.Point(430, 171);
            this.correctAnssLbl.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.correctAnssLbl.Name = "correctAnssLbl";
            this.correctAnssLbl.Size = new System.Drawing.Size(0, 30);
            this.correctAnssLbl.TabIndex = 10;
            // 
            // wrongAnssLbl
            // 
            this.wrongAnssLbl.AutoSize = true;
            this.wrongAnssLbl.Location = new System.Drawing.Point(430, 221);
            this.wrongAnssLbl.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.wrongAnssLbl.Name = "wrongAnssLbl";
            this.wrongAnssLbl.Size = new System.Drawing.Size(0, 30);
            this.wrongAnssLbl.TabIndex = 11;
            // 
            // avgTimePAns
            // 
            this.avgTimePAns.AutoSize = true;
            this.avgTimePAns.Location = new System.Drawing.Point(430, 272);
            this.avgTimePAns.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.avgTimePAns.Name = "avgTimePAns";
            this.avgTimePAns.Size = new System.Drawing.Size(0, 30);
            this.avgTimePAns.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Window;
            this.button1.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(460, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 31);
            this.button1.TabIndex = 13;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MyStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(562, 330);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.avgTimePAns);
            this.Controls.Add(this.wrongAnssLbl);
            this.Controls.Add(this.correctAnssLbl);
            this.Controls.Add(this.gamesPlayedLbl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Ravie", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "MyStatus";
            this.Text = "Trivia";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label gamesPlayedLbl;
        private System.Windows.Forms.Label correctAnssLbl;
        private System.Windows.Forms.Label wrongAnssLbl;
        private System.Windows.Forms.Label avgTimePAns;
        private System.Windows.Forms.Button button1;
    }
}