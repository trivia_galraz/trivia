﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class MyStatus : Form
    {
        NetworkStream clientStream;

        public MyStatus()
        {
            InitializeComponent();
        }

        public MyStatus(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
            GetData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //this function is getting the data from the server and updates the gui accordingly
        private void GetData()
        {
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";

            try
            {
                //send message to server.
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("225"));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                bufferIn = new byte[4096];
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            //chceking input.
            if (input.StartsWith("126")) //if there are no errors, parse input and update labels.
            {
                input = input.Substring(3);

                this.gamesPlayedLbl.Text = int.Parse(input.Substring(0, 4)).ToString();
                input = input.Substring(4);

                this.correctAnssLbl.Text = int.Parse(input.Substring(0, 6)).ToString();
                input = input.Substring(6);

                this.wrongAnssLbl.Text = int.Parse(input.Substring(0, 6)).ToString();
                input = input.Substring(6);

                int avgTime = int.Parse(input.Substring(0, 4));
                string avgTimeStr = avgTime.ToString();

                //casting the avarage time to a float.
                if (avgTimeStr.Length == 3)
                {
                    avgTimeStr = avgTimeStr.Insert(1, ".");
                }
                else if (avgTimeStr.Length == 4)
                {
                    avgTimeStr = avgTimeStr.Insert(2, ".");
                }

                this.avgTimePAns.Text = avgTimeStr;
            }

            else
            {
                MessageBox.Show("Error getting data from server... exiting");
                this.Close();
            }
        }
    }
}
