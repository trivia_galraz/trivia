﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class QuestionAdd : Form
    {
        SpeechRecognitionEngine recognizer;
        bool listen;
        NetworkStream clientStream;

        public QuestionAdd(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;

            //creating a voice recognizer.
            recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
            recognizer.LoadGrammar(new DictationGrammar());
            recognizer.SetInputToDefaultAudioDevice();
            recognizer.RecognizeAsync(RecognizeMode.Multiple);
            recognizer.SpeechRecognized += new EventHandler<System.Speech.Recognition.SpeechRecognizedEventArgs>(recognizer_recognized);
            this.listen = true;
        }

        private void recognizer_recognized(object sender, SpeechRecognizedEventArgs e)
        {
            QuestionText.Text = e.Result.Text;
        }

        private void SendBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string toSend = XORCrypt.EncryptDecrypt("227" + QuestionText.Text.Length.ToString("00") + QuestionText.Text + correctanswer.Text.Length.ToString("00") + correctanswer.Text + ans1.Text.Length.ToString("00") + ans1.Text + ans2.Text.Length.ToString("00") + ans2.Text + ans3.Text.Length.ToString("00") + ans3.Text);
                byte[] buffer = new ASCIIEncoding().GetBytes(toSend);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();

                byte[] bufferIn;
                int bytesRead = 0;
                string input;
                bufferIn = new byte[4];
                bytesRead = clientStream.Read(bufferIn, 0, 4);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));

                //checking input.
                if (input.Equals("1281"))
                {
                    MessageBox.Show("Sorry the question did not create");
                }
                else
                {
                    //asking the user if he want's to add another question.
                    var confirm = MessageBox.Show(" would you like to add another question?", "The question successfully added", MessageBoxButtons.YesNo);
                    //checking user's choice.
                    if (confirm == DialogResult.Yes)
                    {
                        //reseting textboxes.
                        QuestionText.Text = "";
                        correctanswer.Text = "";
                        ans1.Text = "";
                        ans2.Text = "";
                        ans3.Text = "";
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Sorry cant add question");
                this.Close();
            }
        }

        private void ovalPictureBox1_Click(object sender, EventArgs e)
        {
            if (listen)
            {
                //disabling voice recognition.
                listen = false;
                label1.Text = "Disable";
                label1.ForeColor = Color.Red;
                this.recognizer.RecognizeAsyncStop();
            }
            else
            {
                //enabling voice recognition.
                listen = true;
                label1.Text = "Enable";
                label1.ForeColor = Color.Green;
                this.recognizer.RecognizeAsync(RecognizeMode.Multiple);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
