﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontend
{
    class Room //this class represents a room.
    {
        private string _name;
        private string _id;
        public Room(string id, string name)
        {
            this._name = name;
            this._id = id;
        }

        public string Name { get => _name; set => _name = value; }
        public string Id { get => _id; set => _id = value; }
    }
}
