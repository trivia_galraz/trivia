﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class RoomForm : Form
    {
        List<Room> rooms;
        NetworkStream clientStream;
        string selectedRoomName;
        string selectedRoomID;
   
        public RoomForm()
        {
            InitializeComponent();
        }

        public RoomForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
            this.rooms = GetRooms();

            //adding the available rooms to the list.
            for(int i = 0; i < rooms.Count; i++)
            {
                this.AvailableRoomsList.Items.Add(rooms[i].Id + "_" + rooms[i].Name);
            }
        }

        //this fucntion is getting the available rooms from the server, and returns them in a list.
        private List<Room> GetRooms()
        {

            List<Room> rooms = new List<Room>();
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";

            try
            { 
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("205"));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();

                bufferIn = new byte[4096];
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            //parsing server's input into the list of rooms.
            for(int i = 0, j=5; i < int.Parse(input.Substring(3,2));i++)
            {
                rooms.Add(new Room(input.Substring(j,2), input.Substring(j+4,int.Parse(input.Substring(j+2,2)))));
                j += 4 + int.Parse(input.Substring(j + 2, 2));
            }
            return rooms;
        }

        private void AvailableRoomsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.selectedRoomID = AvailableRoomsList.SelectedItem.ToString().Substring(0,2);
                this.selectedRoomName = AvailableRoomsList.SelectedItem.ToString().Substring(3);
            }
            catch
            {
                MessageBox.Show("Room isn't selected");
            }
        }

        //this function updates the available rooms.
        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            this.rooms = this.GetRooms(); //getting available rooms.
            this.AvailableRoomsList.Items.Clear();

            //adding rooms to the list.
            for(int i = 0; i < rooms.Count; i++)
            {
                this.AvailableRoomsList.Items.Add(rooms[i].Id + "_" + rooms[i].Name);
            }
        }

        //this funciton is trying to join the selcted room.
        private void JoinBtn_Click(object sender, EventArgs e)
        {
            if(this.AvailableRoomsList.Items.Count <= 0)
            {
                return;
            }

            if(this.selectedRoomID.Length!=2)
            {
                return;
            }

            string[] data = GetRoomData(this.selectedRoomID);
            if(data[0] != this.selectedRoomID)
            {
                return;
            }
            this.Hide();
            RoomGameForm rgf = new RoomGameForm(clientStream, this.selectedRoomName, data[1], data[2], data[0], false);
            rgf.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        //this function get's the selected room's data from the server.
        public string[] GetRoomData(string id)
        {
            string[] data = new string[3];
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";
            byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("209" + id));
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            bufferIn = new byte[4096];
            bytesRead = clientStream.Read(bufferIn, 0, 4096);
            input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));

            //checking input.
            if(input.StartsWith("1100"))
            { 
                data[0] = id;
                data[1] = int.Parse(input.Substring(4, 2)).ToString();
                data[2] = input.Substring(6, 2).ToString();
                return data;
            }
            else if(input.StartsWith("1101"))
            {
                MessageBox.Show("Sorry room is full");
                return data;
            }
            else
            {
                MessageBox.Show("sorry something went wrong");
                return data;
            }
        }
    }
}
