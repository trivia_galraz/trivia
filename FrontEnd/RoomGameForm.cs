﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class RoomGameForm : Form
    {
        string _roomId;
        NetworkStream clientStream;
        bool admin;
        Thread update;
        List<string[]> questions;
        string name;
        int time;
        bool gmail;

        public RoomGameForm()
        {
            InitializeComponent();
            this.label1.Text += "Example_Room";
            this.label2.Text += "10";
            this.label3.Text += "10";

            this.CurrParticiapents.Items.Add("user");
            this.CurrParticiapents.Items.Add("user2");
        }

        public RoomGameForm(NetworkStream clientStream, string name, string numOfQuestions, string timePerQuestions, string roomId, bool isAdmin)
        {
            this.clientStream = clientStream;
            InitializeComponent();
            this.name = name;
            this._roomId = roomId;
            List<string> users;
            this.time = int.Parse(timePerQuestions);
            this.label1.Text += " " + name;
            this.label2.Text += " " + numOfQuestions;
            this.label3.Text += " " + timePerQuestions;
            this.admin = isAdmin;

            if (!this.admin)
            {
                this.StartGameBtn.Hide();
            }
            else
            {
                this.button1.Text = "Close";
                users = GetUsersInRoom(this._roomId.ToString());

                if (users.Count == 0)
                {
                    MessageBox.Show("Error getting users, room is damaged.");
                }

                for (int i = 0; i < users.Count; i++)
                {
                    this.CurrParticiapents.Items.Add(users[i]);
                }
            }

            update = new Thread(UpdateStatus);
            update.Start();
        }

        private void LeaveRoomBtn_Click(object sender, EventArgs e)
        {
            byte[] buffer;
            if (this.admin) //if the user is an admin, close the room.
            {
                try
                {
                    buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("215"));
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                try
                {
                    //sending a "leave room" request to the server.
                    buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("211"));
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }

        //this function is getting the connected users of this room from the server.
        public List<string> GetUsersInRoom(string id)
        {
            List<string> users = new List<string>();
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";

            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("207" + id));
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();

                bufferIn = new byte[4096];
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            //parsing srever's input.
            for (int i = 0, j = 4; i < int.Parse(input.Substring(3, 1)); i++)
            {
                users.Add(input.Substring(j + 2, int.Parse(input.Substring(j, 2))));
                j += 2 + int.Parse(input.Substring(j, 2));
            }

            return users;
        }

        //this function handles the server messages while waiting for game
        public void UpdateStatus()
        {
            string input;
            int bytesRead;
            byte[] bufferIn = new byte[4096];
            List<string> users = new List<string>();
            this.questions = new List<string[]>();

            while (true)
            {
                clientStream.Flush();
                bytesRead = clientStream.Read(bufferIn, 0, 4096);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
                clientStream.Flush();
                if (input.StartsWith("108"))//user list update
                {
                    try
                    {
                        for (int i = 0, j = 4; i < int.Parse(input.Substring(3, 1)); i++)
                        {
                            users.Add(input.Substring(j + 2, int.Parse(input.Substring(j, 2))));
                            j += 2 + int.Parse(input.Substring(j, 2));
                        }
                        Invoke((MethodInvoker)delegate { this.CurrParticiapents.Items.Clear(); });
                        for (int i = 0; i < users.Count; i++)
                        {
                            Invoke((MethodInvoker)delegate { this.CurrParticiapents.Items.Add(users[i]); });
                        }
                        users.Clear();
                    }
                    catch (Exception)
                    {
                        users.Clear();
                        for (int i = 0, j = 4; i < int.Parse(input.Substring(3, 1)); i++)
                        {
                            users.Add(input.Substring(j + 2, int.Parse(input.Substring(j, 2))));
                            j += 2 + int.Parse(input.Substring(j, 2));
                        }
                        Invoke((MethodInvoker)delegate { this.CurrParticiapents.Items.Clear(); });
                        for (int i = 0; i < users.Count; i++)
                        {
                            Invoke((MethodInvoker)delegate { this.CurrParticiapents.Items.Add(users[i]); });
                        }
                        users.Clear();
                    }
                }
                else if (input.StartsWith("116"))//room closed
                {
                    try
                    {
                        Invoke((MethodInvoker)delegate { this.Close(); });
                        return;
                    }
                    catch
                    {
                        return;
                    }
                }
                else if (input.StartsWith("118"))//game started
                {
                    try
                    {
                        input = input.Replace(";", "");
                        int correctAns = int.Parse(input.Substring(input.Length - 1));
                        GameForm game = new GameForm(this.clientStream, GameQuestions(input.Substring(3)), correctAns, this.time, this);
                        game.ShowDialog();
                        Invoke((MethodInvoker)delegate { this.Close(); });
                        return;
                    }
                    catch (Exception ex)
                    {
                        Invoke((MethodInvoker)delegate { MessageBox.Show(ex.ToString()); });
                        return;
                    }
                }
                else if (input.StartsWith("112"))//leave room
                {
                    return;
                }
            }
        }

        private void StartGameBtn_Click(object sender, EventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(XORCrypt.EncryptDecrypt("217"));
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }

        //this function is parsing the input into an array of strings.
        private string[] GameQuestions(string input)
        {
            string[] data = new string[5];
            for (int i = 0, j = 0; i < 5; i++)
            {
                data[i] = input.Substring(j + 3, int.Parse(input.Substring(j, 3)));
                j += 3 + int.Parse(input.Substring(j, 3));
            }

            return data;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            gmail = true;
            textBox1.Text = "Enter email here";
            textBox1.Visible = true;
            button2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(gmail)
            {
                SendEmail(textBox1.Text);
                MessageBox.Show("Invitation successfully sent");
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            ((TextBox)sender).Text = "";
        }

        //this function is sending an email.
        private void SendEmail(string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("triviarazandgal@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Trivia Invitation";
                mail.Body = "You are invited to join an amazing trivia game at room "+ name + ".\n The Trivia game was made by the extraordinary programmers Gal Peleg and Raz Nauel Yahalom.";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("triviarazandgal@gmail.com", "AaBbCc12");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }

}
