﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class Scores : Form
    {
        public Scores()
        {
            InitializeComponent();
        }

        public Scores(List<KeyValuePair<string, int>> scores)
        {
            InitializeComponent();
            //applying data to lables.
            for (int i = 0; i < scores.Count; i++)
            {
                var current = (Label)this.Controls.Find("label" + (i + 1).ToString(), false).FirstOrDefault();
                current.Visible = true;
                if(current!=null)
                {
                    current.Text = (i + 1) + " " + scores[i].Key.ToString() + " - " + scores[i].Value.ToString();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
