﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class SignIn : Form
    {
        NetworkStream clientStream;
        bool firstUser;
        bool firstPassword;

        public SignIn()
        {
            InitializeComponent();

            string[] data = new string[3];

            try
            {
                //getting data from config file.
                data = File.ReadAllLines("config.txt", Encoding.UTF8);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Data.ToString());
                MessageBox.Show("Can't open config file... closing");
                this.Close();
            }

            //parsing the config file.
            string ipAddr = data[0].Substring(10);
            int portNum = int.Parse(data[1].Substring(5));

            //creating a TCP client.
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ipAddr), portNum);

            try
            {
                client.Connect(serverEndPoint);
            }
            catch (Exception)
            {
                MessageBox.Show("Can't communicate with server...");
                this.Close();
            }

            this.firstUser = true;
            this.firstPassword = true;
            this.clientStream = client.GetStream();
        }

        public SignIn(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
            this.firstUser = true;
            this.firstPassword = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //sending a "exit app" request to the server.
                string toSend = XORCrypt.EncryptDecrypt("299");
                byte[] buffer = new ASCIIEncoding().GetBytes(toSend);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void SignInbtn_Click(object sender, EventArgs e)
        {
            byte[] bufferIn;
            int bytesRead = 0;
            string input = "";

            try
            {
                //sending a "sign in" request to the server.
                string toSend = XORCrypt.EncryptDecrypt("200" + UsernameText.Text.Length.ToString("00") + UsernameText.Text + PasswordText.Text.Length.ToString("00") + PasswordText.Text);
                byte[] buffer = new ASCIIEncoding().GetBytes(toSend);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();

                bufferIn = new byte[4];
                bytesRead = clientStream.Read(bufferIn, 0, 4);
                input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            //checking the server's input.
            if (input == "1020")
            {
                Menu mn = new Frontend.Menu(this.clientStream);
                this.Hide();
                mn.ShowDialog();
            }
            else if (input == "1021")
            {
                MessageBox.Show("Wrong details");
            }
            else if (input == "1022")
            {
                MessageBox.Show("User is allready conected");
            }
            else
            {
                MessageBox.Show("Something went wrong");
            }
        }

        private void PasswordText_TextChanged(object sender, EventArgs e)
        {
            PasswordText.PasswordChar = '*';
        }

        //this funtion is checking if the username text is valid.
        private bool ValidUserName(string username)
        {
            if (username.Length < 3)
            {
                return false;
            }
            if (username.IndexOf(' ') != -1)
            {
                return false;
            }
            if (!Char.IsLetter(username[0]))
            {
                return false;
            }
            return true;
        }

        //this funtion is checking if the password text is valid.
        private bool ValidPassword(string password)
        {
            if (password.Length < 3)
            {
                return false;
            }
            bool upper = false;
            bool lower = false;
            bool number = false;
            bool space = password.IndexOf(' ') == -1;

            //checking for a digit, a uppercase letter and a lowercase letter.
            for (int i = 0; i < password.Length; i++)
            {
                if (Char.IsDigit(password[i]))
                {
                    number = true;
                }
                else if (Char.IsLower(password[i]))
                {
                    lower = true;
                }
                else if (Char.IsUpper(password[i]))
                {
                    upper = true;
                }
            }

            return number && lower && upper && space;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            SignUpForm wnd = new SignUpForm(clientStream);
            this.Hide();
            wnd.ShowDialog();
            this.Show();
        }

        private void UsernameText_Click(object sender, EventArgs e)
        {
            if(firstUser)
            {
                UsernameText.Text = "";
                firstUser = false;
            }
        }

        private void PasswordText_Click(object sender, EventArgs e)
        {
            if(firstPassword)
            {
                PasswordText.Text = "";
                firstPassword = false;
                PasswordText.PasswordChar = '*';
            }
        }
    }
}
