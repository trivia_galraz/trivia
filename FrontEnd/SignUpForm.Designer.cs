﻿namespace Frontend
{
    partial class SignUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUpForm));
            this.UsernameText = new System.Windows.Forms.TextBox();
            this.EmailText = new System.Windows.Forms.TextBox();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.Submit = new System.Windows.Forms.Button();
            this.UserNamePic = new System.Windows.Forms.PictureBox();
            this.PasswordPic = new System.Windows.Forms.PictureBox();
            this.EmailPic = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.UserNamePic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // UsernameText
            // 
            this.UsernameText.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameText.Location = new System.Drawing.Point(12, 46);
            this.UsernameText.Name = "UsernameText";
            this.UsernameText.Size = new System.Drawing.Size(247, 26);
            this.UsernameText.TabIndex = 5;
            this.UsernameText.Text = "UserName";
            this.UsernameText.Click += new System.EventHandler(this.UsernameText_Click);
            this.UsernameText.TextChanged += new System.EventHandler(this.UsernameText_TextChanged);
            // 
            // EmailText
            // 
            this.EmailText.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailText.Location = new System.Drawing.Point(12, 110);
            this.EmailText.Name = "EmailText";
            this.EmailText.Size = new System.Drawing.Size(247, 26);
            this.EmailText.TabIndex = 7;
            this.EmailText.Text = "Email";
            this.EmailText.Click += new System.EventHandler(this.EmailText_Click);
            this.EmailText.TextChanged += new System.EventHandler(this.EmailText_TextChanged);
            // 
            // PasswordText
            // 
            this.PasswordText.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordText.Location = new System.Drawing.Point(12, 78);
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.Size = new System.Drawing.Size(247, 26);
            this.PasswordText.TabIndex = 6;
            this.PasswordText.Text = "Password";
            this.PasswordText.Click += new System.EventHandler(this.PasswordText_Click);
            this.PasswordText.TextChanged += new System.EventHandler(this.PasswordText_TextChanged);
            // 
            // Submit
            // 
            this.Submit.BackColor = System.Drawing.SystemColors.Window;
            this.Submit.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Submit.Location = new System.Drawing.Point(65, 173);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(123, 36);
            this.Submit.TabIndex = 8;
            this.Submit.Text = "SignUp!";
            this.Submit.UseVisualStyleBackColor = false;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // UserNamePic
            // 
            this.UserNamePic.BackColor = System.Drawing.SystemColors.Window;
            this.UserNamePic.Location = new System.Drawing.Point(237, 50);
            this.UserNamePic.Margin = new System.Windows.Forms.Padding(6);
            this.UserNamePic.Name = "UserNamePic";
            this.UserNamePic.Size = new System.Drawing.Size(19, 19);
            this.UserNamePic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.UserNamePic.TabIndex = 9;
            this.UserNamePic.TabStop = false;
            // 
            // PasswordPic
            // 
            this.PasswordPic.BackColor = System.Drawing.SystemColors.Window;
            this.PasswordPic.Location = new System.Drawing.Point(237, 82);
            this.PasswordPic.Margin = new System.Windows.Forms.Padding(6);
            this.PasswordPic.Name = "PasswordPic";
            this.PasswordPic.Size = new System.Drawing.Size(19, 19);
            this.PasswordPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PasswordPic.TabIndex = 9;
            this.PasswordPic.TabStop = false;
            // 
            // EmailPic
            // 
            this.EmailPic.BackColor = System.Drawing.SystemColors.Window;
            this.EmailPic.Location = new System.Drawing.Point(237, 114);
            this.EmailPic.Margin = new System.Windows.Forms.Padding(6);
            this.EmailPic.Name = "EmailPic";
            this.EmailPic.Size = new System.Drawing.Size(19, 19);
            this.EmailPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.EmailPic.TabIndex = 9;
            this.EmailPic.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(223, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SignUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(268, 248);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.EmailPic);
            this.Controls.Add(this.PasswordPic);
            this.Controls.Add(this.UserNamePic);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.PasswordText);
            this.Controls.Add(this.EmailText);
            this.Controls.Add(this.UsernameText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignUpForm";
            ((System.ComponentModel.ISupportInitialize)(this.UserNamePic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox UsernameText;
        private System.Windows.Forms.TextBox EmailText;
        private System.Windows.Forms.TextBox PasswordText;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.PictureBox UserNamePic;
        private System.Windows.Forms.PictureBox PasswordPic;
        private System.Windows.Forms.PictureBox EmailPic;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}