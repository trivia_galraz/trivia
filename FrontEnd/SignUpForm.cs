﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class SignUpForm : Form
    {
        NetworkStream clientStream;
        public SignUpForm()
        {
            InitializeComponent();
           
        }
        public SignUpForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this.clientStream = clientStream;
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            //sending a "sign up" request to the server.
            string toSend = XORCrypt.EncryptDecrypt("203" + UsernameText.Text.Length.ToString("00") + UsernameText.Text + PasswordText.Text.Length.ToString("00") + PasswordText.Text + EmailText.Text.Length.ToString("00") + EmailText.Text);
            byte[] buffer = new ASCIIEncoding().GetBytes(toSend);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            byte[] bufferIn;
            int bytesRead = 0;
            string input;
            bufferIn = new byte[4];
            bytesRead = clientStream.Read(bufferIn, 0, 4);
            input = XORCrypt.EncryptDecrypt(new ASCIIEncoding().GetString(bufferIn));

            //checking server's input.
            if (input == "1040")
            {
                MessageBox.Show("New user signed up");
                this.Close();
            }
            else if (input == "1041")
            {
                MessageBox.Show("Password is illeagal");
            }
            else if (input == "1042")
            {
                MessageBox.Show("User is allready exist");
            }
            else if(input == "1043")
            {
                MessageBox.Show("Username is illeagal");
            }
            else
            {
                MessageBox.Show("Something went wrong");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void UsernameText_TextChanged(object sender, EventArgs e)
        {
            if(ValidUserName(UsernameText.Text))
            {
                UserNamePic.Image = Properties.Resources.valid;
            }
            else
            {
                UserNamePic.Image = Properties.Resources.invalid;
            }   
        }

        private void PasswordText_TextChanged(object sender, EventArgs e)
        {
            if (ValidPassword(PasswordText.Text))
            {
                PasswordPic.Image = Properties.Resources.valid;
            }
            else
            {
                PasswordPic.Image = Properties.Resources.invalid;
            }
        }

        private void EmailText_TextChanged(object sender, EventArgs e)
        {
            if (ValidEmail(EmailText.Text))
            {
                EmailPic.Image = Properties.Resources.valid;
            }
            else
            {
                EmailPic.Image = Properties.Resources.invalid;
            }
        }

        //this funtion is checking if the username text is valid.
        private bool ValidUserName(string username)
        {
            if (username.Length < 3)
            {
                return false;
            }
            if (username.IndexOf(' ') != -1)
            {
                return false;
            }
            if (!Char.IsLetter(username[0]))
            {
                return false;
            }
            return true;
        }

        //this funtion is checking if the email text is valid.
        private bool ValidEmail(string email)
        {
            if (email.Length < 6)
            {
                return false;
            }
            if (email.IndexOf('@') == -1 || email.IndexOf('.') == -1)
            {
                return false;
            }
            if (!Char.IsLetter(email[0])||!Char.IsLetter(email[email.Length-1]))
            {
                return false;
            }
            return true;
        }

        //this funtion is checking if the password text is valid.
        private bool ValidPassword(string password)
        {
            if (password.Length < 3)
            {
                return false;
            }
            bool upper = false;
            bool lower = false;
            bool number = false;
            bool space = password.IndexOf(' ') == -1;

            //checking for a digit, a uppercase letter and a lowercase letter.
            for (int i = 0; i < password.Length; i++)
            {
                if (Char.IsDigit(password[i]))
                {
                    number = true;
                }
                else if (Char.IsLower(password[i]))
                {
                    lower = true;
                }
                else if (Char.IsUpper(password[i]))
                {
                    upper = true;
                }
            }
            return number && lower && upper && space;
        }

        private void UsernameText_Click(object sender, EventArgs e)
        {
            UsernameText.Text = "";
        }

        private void PasswordText_Click(object sender, EventArgs e)
        {
            PasswordText.Text = "";
        }

        private void EmailText_Click(object sender, EventArgs e)
        { 
            EmailText.Text = "";
        }
    }
}
